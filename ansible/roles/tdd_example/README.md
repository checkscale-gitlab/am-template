Role Name
=========

This is a TDD example project to check that all usage is done.

Requirements
------------

Does not have requirements.

Role Variables
--------------

This role does not have external variables.

Dependencies
------------

This role does not have any dependency.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - tdd_example

License
-------

BSD
